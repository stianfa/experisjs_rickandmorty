import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter} from 'react-router-dom';
import {Route} from 'react-router-dom';
import CardPage from './Containers/CharacterCards/CardPage/CardPage';
import CardsPage from './Containers/CharacterCards/CardsPage/CardsPage';
import LocationPage from './Containers/LocationCards/LocationPage/LocationPage';
import LocationsPage from './Containers/LocationCards/LocationsPage/LocationsPage';
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(
    <BrowserRouter>
        <App>
            <Route exact path="/" component={CardsPage}></Route>
            <Route path="/character/:id" component={CardPage}></Route>
            <Route path="/locations" component={LocationsPage}></Route>
            <Route path="/locations/:id" component={LocationPage}></Route>
        </App>
    </BrowserRouter>, 
    
    document.getElementById('root')
);


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
