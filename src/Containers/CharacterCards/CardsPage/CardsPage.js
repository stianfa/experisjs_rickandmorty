import React from 'react';
import CharacterCard from '../../../Components/CharacterCard/CharacterCard.js'
import SearchComponent from '../../../Components/SearchComponent/SearchComponent.js'

class CardsPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            rickMorty: [],
            characterCards: [],
        }
    }


    getData = (searchInput) => {
        let apiUrl = "https://rickandmortyapi.com/api/character/"
        fetch(apiUrl)
        .then(response => response.json())
        .then(data => {
            if(data.results){
                this.setState({characterCards: data.results})
                console.log("api url: " + apiUrl);
            }
            else {
                console.log("no data - CardsPage getData()");
            }
        });  
    }
    
    
    componentDidMount() {
        this.getData("");
    }


    render() {
        let characters = null;
        if(this.state.characterCards.length > 0) {
            characters = this.state.characterCards.map(character => (
                <CharacterCard
                    key={character.id}
                    id={character.id}
                    image={character.image}
                    name={character.name}
                    species={character.species}
                    status={character.status}
                    gender={character.gender}
                    location={character.location.name}
                    origin={character.origin.name}
                    showLink={true}>
                </CharacterCard>
            ));
        }
        else {
            characters = <p>Loading...</p>
        }
        
        return (
            <React.Fragment>
                <header>
                    <img alt="banner" src="https://i2.wp.com/www.pixelcrumb.com/wp-content/uploads/2016/10/RICK-AND-MORTY-BANNER.jpg?fit=1920%2C720"
                        style={{
                            margin: 0,
                            display: "block", 
                            width: "100%"
                        }}
                    ></img>
                
                </header>

                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <a href="/">Characters</a> |
                    <a href="/locations/">Locations</a> |
                </nav>

                <h4 style={{textAlign:"center", marginTop: "20px", fontSize: "38px"}}>Characters</h4>
                <br />
                <SearchComponent onChange={(searchInput) => this.getData(searchInput)}/>
                <br />
                <div className="row" style={{
                            margin: "auto", 
                            width: "75%"
                        }}>    
                    {characters}
                </div>
            </React.Fragment>
        );
    }
}


export default CardsPage;