import React from 'react';
import CharacterCard from "../../../Components/CharacterCard/CharacterCard.js"

class CardPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            character: {}
        }
    }

    getCharacterData = () => {
        fetch("https://rickandmortyapi.com/api/character/" + this.props.match.params.id)
        .then(response => response.json())
        .then(data => { 
            console.log(data);
            
            this.setState({
                character: data
            }) 
        })
    }

    componentDidMount() {
        this.getCharacterData();
    }

    render() {
        let characterCard  = null;

        if(this.state.character.id) {
            characterCard  = (
            <CharacterCard                
                key={this.state.character.id}
                id={this.state.character.id}
                image={this.state.character.image}
                name={this.state.character.name}
                species={this.state.character.species}
                status={this.state.character.status}
                gender={this.state.character.gender}
                location={this.state.character.location.name}
                origin={this.state.character.origin.name}>
            </CharacterCard>);
        }
        else {
            characterCard  = <p>Loading card...</p>
        }
        return (
            <React.Fragment>
                <h4>Characters</h4>
                <br />
                <div className="row">
                    {characterCard }
                </div>
            </React.Fragment>
        );
    }
    
}



export default CardPage;