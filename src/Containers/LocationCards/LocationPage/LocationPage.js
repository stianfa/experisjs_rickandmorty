import React from 'react';
import LocationCard from "../../../Components/LocationCard/LocationCard.js"

class LocationPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            location: {}
        }
    }

    getCharacterData = () => {
        fetch("https://rickandmortyapi.com/api/location/")
        .then(response => response.json())
        .then(data => { 
            console.log(data);
            
            this.setState({
                location: data
            }) 
        })
    }

    componentDidMount() {
        this.getCharacterData();
    }

    render() {
        let locationCard  = null;

        if(this.state.character.id) {
            locationCard  = (
            <LocationCard                
                key={this.state.location.id}
                id={this.state.location.id}
                name={this.state.location.name}
                type={this.state.location.type}
                dimension={this.state.location.dimension}
                residents={this.state.location.residents}
                url={this.state.location.url}
                origin={this.state.location.origin.name}>
            </LocationCard>);
        }
        else {
            locationCard  = <p>Loading locations...</p>
        }
        return (
            <React.Fragment>
                <h4>Locations</h4>
                <br />
                <div className="row">
                    {locationCard }
                </div>
            </React.Fragment>
        );
    }
    
}



export default LocationPage;