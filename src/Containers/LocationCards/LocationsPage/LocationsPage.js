import React from 'react';
import LocationCard from '../../../Components/LocationCard/LocationCard.js'
import SearchComponent from '../../../Components/SearchComponent/SearchComponent.js'

class LocationCards extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            rickMorty: [],
            locationCards: [],
        }
    }


    getData = (searchInput) => {
        let apiUrl = "https://rickandmortyapi.com/api/location?=" + searchInput
        fetch(apiUrl)
        .then(response => response.json())
        .then(data => {
            if(data.results){
                this.setState({locationCards: data.results})
                console.log("api url: " + apiUrl);
            }
            else {
                console.log("no data - LocationsPage getData()");
            }
        });  
    }
    
    
    componentDidMount() {
        this.getData("");
    }


    render() {
        let locations = null;
        if(this.state.locationCards.length > 0) {
            locations = this.state.locationCards.map(location => (
                <LocationCard
                    key={location.id}
                    id={location.id}
                    name={location.name}
                    type={location.type}
                    dimension={location.dimension}
                    residents={location.residents}
                    url={location.url}>
                </LocationCard>
            ));
        }
        else {
            locations = <p>Loading...</p>
        }
        
        return (
            <React.Fragment>
                <header>
                    <img alt="banner" src="https://i2.wp.com/www.pixelcrumb.com/wp-content/uploads/2016/10/RICK-AND-MORTY-BANNER.jpg?fit=1920%2C720"
                        style={{
                            margin: 0,
                            display: "block", 
                            width: "100%"
                        }}
                    ></img>
                
                </header>

                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <a href="/">Characters</a> |
                    <a href="/locations/">Locations</a> |
                </nav>

                <h4 style={{textAlign:"center", marginTop: "20px", fontSize: "38px"}}>Locations</h4>
                <br />

                <div className="row" style={{
                            margin: "auto", 
                            width: "75%"
                        }}>    
                    {locations}
                </div>
            </React.Fragment>
        );
    }
}


export default LocationCards;