import React from 'react';
import {Link} from "react-router-dom";

let linkButton = null;

const characterCard = (props) => {
    
    linkButton = 
        <Link to={{ pathname: "/character/" + props.id, }} className="btn btn-primary"> 
            View
        </Link>
   
    
    return (
        <div className="col-xs-4" style={{width: "15rem"}}>
            <div className="card border mb-3 CharacterCard" >
                <img 
                    src={props.image}
                    alt={props.name}
                    className="card-img-top"
                />

                <div className="card-body">
                    <h5 className="card-title">{props.name}</h5>
                    <b>Species: </b> {props.species} <br />
                    <b>Status: </b> {props.status} <br />
                    <b>Gender: </b> {props.gender} <br />
                    <b>Location: </b> {props.location} <br />
                    <b>Place of origin: </b> {props.origin} <br />
                    {props.showLink ? linkButton : null}
                    <br />
                </div>
            </div>
        </div>
    );
}

export default characterCard;