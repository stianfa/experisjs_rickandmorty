
import React, {Component} from 'react';


class SearchComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            searchInput: ''
        }

        this.handleCharacterSearch = this.handleCharacterSearch.bind(this)
        this.updateSearchText = this.updateSearchText.bind(this)
    }


    handleCharacterSearch(e) {
        if(this.props.onChange) {
            this.props.onChange(this.state.searchInput)
        }    
    }


    updateSearchText(e) {
        this.setState({searchInput : e.target.value})     
    }


    render() {
        return (
            <div className="input-group" 
            style={{
                margin: "auto", 
                width: "75%"
            }}>             
                <input 
                    onChange={this.updateSearchText}
                    type="text"
                    className="form-control"
                    placeholder="Search for a Rick & Morty character" 
                />
                <button type="button" className="btn btn-outline-secondary" onClick={this.handleCharacterSearch}>Search</button>
            </div>
        )
    }
}

export default SearchComponent