import React from 'react';
import {Link} from "react-router-dom";

let linkButton = null;

const LocationCard = (props) => {
    
    linkButton = 
        <Link to={{ pathname: props.url, }} className="btn btn-primary"> 
            View
        </Link>
   
    
    return (
        <div className="col-xs-4" style={{width: "15rem"}}>
            <div className="card border mb-3 CharacterCard" >
                <div className="card-body">
                    <h5 className="card-title">{props.name}</h5>
                    <b>type: </b> {props.type} <br />
                    <b>dimension: </b> {props.dimension} <br />

                    {props.showLink ? linkButton : null}
                    <br />
                </div>
            </div>
        </div>
    );
}

export default LocationCard;